// setup the dependencies
const express = require ('express');
const mongoose = require ('mongoose');
// this allows us to use all the routes defined in 'taskRoute.js'
const taskRoute = require('./routes/taskRoute');


// server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}))

// database connection
mongoose.connect("mongodb+srv://steventroy:steventroy@wdc028-course-booking.alyii.mongodb.net/batch-144-database?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", ()=> console.log ("we're connected to the cloud database"))

// Routes (base uri for task route)
// allows all the task routes created in the taskRoute.js file to use "/tasks" route

app.use("/tasks", taskRoute)
//http://localhost:3001/tasks


app.listen(port, () => console.log(`now listening to port ${port}`))

